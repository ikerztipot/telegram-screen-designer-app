# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import CustomUser

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<username>/<filename>
    return 'user_{0}/{1}'.format(instance.project.user.username, filename)


class Project(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(_('name'), max_length=100, blank=False, null=False, help_text=_('Write a recognizable name for the project'))
    description = models.CharField(_('description'), max_length=250, blank=True, null=True, help_text=_('Write something about the project'))
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True)
    user = models.ForeignKey(CustomUser, to_field='email', on_delete=models.CASCADE, db_column='user', blank=False, null=False)
    thumbnail = models.URLField(blank=False, null=False, default='user_default/no-img.png')
    screen_number = models.PositiveIntegerField(default=0, blank=False, null=False)
    #shared with users

    class Meta:
        db_table = 'project'
        verbose_name = _('project')
        verbose_name_plural = _('projects')

    def __str__(self):
	    return self.name

    def __unidoce__(self):
	    return self.name

    def save(self, *args, **kwargs):
        self.screen_number = Screen.objects.filter(project=self).count()
        super(Project, self).save(*args, **kwargs)

class Screen(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(_('name'), max_length=100, blank=False, null=False, help_text=_('Write a recognizable name for the screen'))
    data = models.TextField(_('data'), blank=True, null=True)
    description = models.CharField(_('description'), max_length=250, blank=True, null=True, help_text=_('Write something about the screen'))
    created_at = models.DateTimeField(blank=False, null=False, auto_now_add=True)
    updated_at = models.DateTimeField(blank=False, null=False, auto_now=True)
    project = models.ForeignKey(Project, to_field='id', on_delete=models.CASCADE, db_column='project', blank=False, null=False)
    thumbnail = models.ImageField(upload_to=user_directory_path, default='user_default/no-img.png')

    class Meta:
        db_table = 'screen'
        verbose_name = _('screen')
        verbose_name_plural = _('screens')

    def __str__(self):
	    return self.name

    def __unidoce__(self):
	    return self.name

    def save(self, *args, **kwargs):
        try:
            this = Screen.objects.get(id=self.id)
            if this.thumbnail != self.thumbnail:
                this.thumbnail.delete()
        except: pass
        super(Screen, self).save(*args, **kwargs)
