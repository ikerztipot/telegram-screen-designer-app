# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TelegramScreenDesignerConfig(AppConfig):
    name = 'telegram_screen_designer'
