from django.conf.urls import url
from telegram_screen_designer import views
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(_(r'^$'), views.index, name='index'),
    url(_(r'^download-screenshot/'), views.download_screenshot , name='download_screenshot'),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/project/(?P<project_id>[a-zA-Z0-9_.-]+)/screen/(?P<screen_id>[a-zA-Z0-9_.-]+)/save-screen$'), views.save_screen , name='save_screen'),
    url(_(r'^sign-up$'), views.user_register, name='register'),
    url(_(r'^log-in$'), views.user_login, name="login"),
    url(_(r'^logout$'), views.user_logout, name="logout"),
    url(_(r'^change-language/(?P<language>[a-zA-Z]+)$'), views.change_language, name="change_language"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/projects$'), views.my_projects_page, name="my_projects"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/project/(?P<project_id>[a-zA-Z0-9_.-]+)$'), views.project_page, name="project"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/project/(?P<project_id>[a-zA-Z0-9_.-]+)/delete$'), views.delete_project, name="delete_project"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/project/(?P<project_id>[a-zA-Z0-9_.-]+)/screen/(?P<screen_id>[a-zA-Z0-9_.-]+)/delete$'), views.delete_screen, name="delete_screen"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/new-project$'), views.new_project, name="new_project"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/project/(?P<project_id>[a-zA-Z0-9_.-]+)/new-screen$'), views.new_screen, name="new_screen"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/project/(?P<project>[a-zA-Z0-9_.-]+)/screens$'), views.project_screens, name="project_screens"),
    url(_(r'^(?P<username>[a-zA-Z0-9_.-]+)/project/(?P<project_id>[a-zA-Z0-9_.-]+)/screen/(?P<screen_id>[a-zA-Z0-9_.-]+)$'), views.screen_page, name="screen")
]
