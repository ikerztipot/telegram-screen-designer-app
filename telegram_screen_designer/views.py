# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#django imports
from django.shortcuts import render, redirect
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils import translation
from core.models import CustomUser
from telegram_screen_designer.models import Project, Screen
from telegram_screen_designer.forms import UserForm, LoginForm, ProjectForm, ScreenForm
from telegram_screen_designer.base64ToImageField import decode_base64_file
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.utils.timezone import now
from django.conf import settings


#python imports
import base64
import logging
import os

import logging.config
logging.config.dictConfig(settings.LOGGING)

# available languages
AVAILABLE_LANGUAGES = ['en', 'es', 'eu']

# get an instance of a logger
logger = logging.getLogger('telegram-screen-designer')

# Create your views here.
def index(request):
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    return render(request, 'telegram_screen_designer/index.html', context = context_dict)

def download_screenshot(request):
    logger.info("Image received");
    image_64_decode = base64.decodestring(request.POST.get("screen-img", "").split(',')[1])
    #TODO: IMPROVE this action and do it in user browser (with js)
    return HttpResponse(image_64_decode, content_type="image/jpeg")

def user_register(request):
    registered = False
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
            login(request, user)
            return HttpResponseRedirect(reverse('my_projects', kwargs={'username': request.user.username}))
        else:
            print(user_form.errors)
            #messages.error(request, user_form.errors)
    else:
        user_form = UserForm()

    return render(request, 'telegram_screen_designer/register.html', {'form':user_form, 'registered':registered})

def user_login(request):
    logger.debug("user_login view reached!")
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    login_form = LoginForm(request.POST or None)
    if request.method == 'POST':
        if login_form.is_valid():
            email = login_form.cleaned_data['email']
            password = login_form.cleaned_data['password']
            user = authenticate(email=email, password=password)
            logger.debug("Login request. User: " + login_form.cleaned_data['email'])
            if user:
                if user.is_active:
                    login(request, user)
                    logger.debug("User " + user.email + " logged succesfully")
                    return HttpResponseRedirect(reverse('my_projects', kwargs={'username': request.user.username}))
                else:
                    login_form.add_error(None, "Your account is dissabled")
                    # Translators: This message appears on the sign in page only
                    #messages.warning(request, _("Your account is dissabled")
            else:
                logger.debug("Incorrect login data for user: " + login_form.cleaned_data['email'])
                login_form.add_error(None, "Invalid login details")
                login_form.add_error("email", "")
                login_form.add_error("password", "")
                # Translators: This message appears on the sign in page only
                #messages.error(request, _("Invalid login details"))
    context_dict['form'] = login_form
    return render(request, 'telegram_screen_designer/login.html', context_dict)

@login_required
def user_logout(request):
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    logout(request)
    return HttpResponseRedirect(reverse('index'))

@login_required
def my_projects_page(request, username):
    try:
        context_dict ={}
        context_dict['available_languages'] = AVAILABLE_LANGUAGES
        projects = Project.objects.filter(user=request.user).order_by('-updated_at')
        context_dict['projects'] = projects
        #For each project obtain the first 4 screens, order by 'update_at'
        logger.debug('PROJECTS')
        for project in projects:
            logger.debug('Project loop, project: project_' + str(project.id) + '_screens')
            screens = Screen.objects.filter(project=project).order_by('-updated_at')
            context_dict['project_' + str(project.id) + '_screens'] = screens

    except Project.DoesNotExist:
        logger.debug("NO projects with " + username)
        context_dict['projects'] = None
    context_dict['now'] = now()
    return render(request, 'telegram_screen_designer/my_projects.html', context_dict)

@login_required
def project_page(request, username, project_id):
    context_dict ={}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    try:
        project = Project.objects.get(id=project_id)
        context_dict['project'] = project
        screens = Screen.objects.filter(project=project_id).order_by('-updated_at')
        context_dict['screens'] = screens
    except Screen.DoesNotExist:
        logger.debug("NO screens with " + username)
        context_dict['screens'] = None
    except Project.DoesNotExist:
        logger.debug("NO project with id " + project_id)

    context_dict['now'] = now()
    return render(request, 'telegram_screen_designer/project.html', context_dict)

@login_required
def new_project(request, username):
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    project_form = ProjectForm(data=request.POST or None)
    if request.method == 'POST':
        if project_form.is_valid():
            project = project_form.save(commit=False)
            project.user = request.user
            project.save()
            messages.success(request, "Your project has been created.")
            return HttpResponseRedirect(reverse('project', kwargs={'username': project.user.username, 'project_id': project.id}))
        else:
            print(project_form.errors)
            messages.error(request, "Please correct the error below.")
    context_dict['form'] = project_form
    return render(request, 'telegram_screen_designer/new_project.html', context_dict)

@login_required
def delete_project(request, username, project_id):
    if request.method == "POST":
        project = Project.objects.get(id=project_id)
        project.delete()
        messages.success(request, "Project " + project.name + " has been deleted")

    return HttpResponseRedirect(reverse('my_projects', kwargs={'username': username}))

@login_required
def delete_screen(request, username, project_id, screen_id):
    if request.method == "POST":
        screen = Screen.objects.get(id=screen_id)
        screen.delete()
        messages.success(request, "Screen " + screen.name + " has been deleted")

    return HttpResponseRedirect(reverse('project', kwargs={'username': username, 'project_id': project_id}))

@login_required
def new_screen(request, username, project_id):
    logger.info("NEW SCREEN REQUEST")
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    screen_form = ScreenForm(data=request.POST or None)
    if request.method == 'POST':
        if screen_form.is_valid():
            screen = screen_form.save(commit=False)
            screen.project = Project.objects.get(id=project_id, user=request.user)
            screen.save()
            #Save project for update the updated_at field automatically
            screen.project.save()
            messages.success(request, "Your screen has been created.")
            return HttpResponseRedirect(reverse('screen', kwargs={'username': screen.project.user.username, 'project_id': project_id, 'screen_id': screen.id}))
        else:
            messages.error(request, "Please correct the error below.")
    context_dict['project_id'] = project_id
    context_dict['form'] = screen_form
    return render(request, 'telegram_screen_designer/new_screen.html', context_dict)

@login_required
def project_screens(request, username, project):
    empty()

@login_required
def screen_page(request, username, project_id, screen_id):
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    screen = Screen.objects.get(id=screen_id, project=project_id)
    screen_list = Screen.objects.filter(project=project_id).order_by('-updated_at')

    # Get previous and next screen. get_next_by_FIELD gets list desc so we swap the previos and next names
    try:
        context_dict['previous_screen'] = screen.get_next_by_updated_at(project=project_id)
    except Screen.DoesNotExist:
        context_dict['previous_screen'] = None

    try:
        context_dict['next_screen'] = screen.get_previous_by_updated_at(project=project_id)
    except Screen.DoesNotExist:
        context_dict['next_screen'] = None

    context_dict['screen'] = screen
    context_dict['screen_list'] = screen_list
    context_dict['project_id'] = project_id
    return render(request, 'telegram_screen_designer/screen.html', context_dict)

@login_required
def save_screen(request, username, project_id, screen_id):
    logger.debug("save_screen view reached!")
    context_dict = {}
    context_dict['available_languages'] = AVAILABLE_LANGUAGES
    # get screen snapshot in base64, convert to jpg and save
    thumbnail_base64 = request.POST.get("screen-img", "")
    decoded_thumbnail = decode_base64_file(thumbnail_base64, 'screen_{1}_thumbnail'.format(request.user.username, screen_id))
    logger.debug("Image succesfully decoded!")
    json_screen = request.POST.get("screen-json-data", "")
    screen = Screen.objects.get(id=screen_id, project=project_id)
    logger.debug("Screen " + str(screen.id) + " correctly obtained from database.")
    screen.data = json_screen
    screen.thumbnail = decoded_thumbnail
    screen.save()
    # saving for update updated_at date
    project = Project.objects.get(id=project_id)
    project.save()
    logger.debug("Screen " + str(screen.id) + " correctly saved into database.")
    messages.success(request, "Screen saved correctly!")
    context_dict['project_id'] = project_id
    context_dict['screen'] = screen
    return HttpResponseRedirect(reverse('screen', kwargs={'username': screen.project.user.username, 'project_id': project_id, 'screen_id': screen.id}))
    #return render(request, 'telegram_screen_designer/screen.html', context_dict)

def change_language(request, language):
    translation.activate(language)
    request.session[translation.LANGUAGE_SESSION_KEY] = language
    return HttpResponseRedirect(request.get_full_path)
