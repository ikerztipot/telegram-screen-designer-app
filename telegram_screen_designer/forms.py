from django import forms
from core.models import CustomUser
from telegram_screen_designer.models import Project, Screen

class UserForm(forms.ModelForm):
    email = forms.EmailField(max_length=254, widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'password', 'first_name', 'last_name')

class LoginForm(forms.Form):
    email = forms.EmailField(required=True, max_length=254, widget=forms.EmailInput(attrs={'id':'InputEmail', 'name':'email', 'aria-describedby':'emailHelp', 'placeholder':'Enter email'}))
    password = forms.CharField(required=True, widget=forms.PasswordInput(attrs={'id':'InputPassword','name':'password', 'placeholder':'Password'}))

#class UserUpdateForm(forms.ModelForm):
#    email = forms.EmailField(max_length=254, widget=forms.EmailInput(attrs={'class': 'form-control', 'readonly': True}))
#    about = forms.CharField(widget=forms.Textarea(), required=False)
#    class Meta:
#        model = CustomUser
#        fields = ('email', 'first_name', 'last_name', 'about')

class ProjectForm(forms.ModelForm):
    name = forms.CharField(max_length=100)
    description = forms.CharField(max_length=250)

    class Meta:
        model = Project
        fields = ('name', 'description')

class ScreenForm(forms.ModelForm):
    name = forms.CharField(max_length=100)
    description = forms.CharField(max_length=250)

    class Meta:
        model = Screen
        fields = ('name', 'description')
