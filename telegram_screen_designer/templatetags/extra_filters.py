from django import template

register = template.Library()

@register.filter(name='addstr')
def addstr(arg1, arg2):
    """concatenate arg1 & arg2"""
    return str(arg1) + str(arg2)

@register.filter(name='lookup')
def lookup(object, property):
    return getattr(object, property)

@register.filter(name='getkey')
def getkey(d, key):
    return d.get(key, '')
