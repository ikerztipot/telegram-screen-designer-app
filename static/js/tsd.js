var SCREEN_BODY_WIDTH = 380; //px
var SCREEN_BODY_HEIGHT = 500; //px

var ANDROID_TOP_FRAME_URL = 'http://localhost/telegram-screen-designer/img/nexus-5x-top.png';
var ANDROID_MIDDLE_FRAME_URL = 'http://localhost/telegram-screen-designer/img/nexus-5x-middle.png';
var ANDROID_BOTTOM_FRAME_URL = 'http://localhost/telegram-screen-designer/img/nexus-5x-bottom.png';

var IPHONE_TOP_FRAME_URL = 'http://localhost/telegram-screen-designer/img/iphone-8-top.png';
var IPHONE_MIDDLE_FRAME_URL = 'http://localhost/telegram-screen-designer/img/iphone-8-middle.png';
var IPHONE_BOTTOM_FRAME_URL = 'http://localhost/telegram-screen-designer/img/iphone-8-bottom.png';

var COMMON_KEYBOARD_GENERATOR_CLASS = "generated-keyboard";
var COMMON_KEYBOARD_SCREEN_CLASS = "screen-generated-keyboard";
var INLINE_KEYBOARD_GENERATOR_CLASS = "generated-inline-keyboard";
var INLINE_KEYBOARD_BASE_CLASS = "inlinekeyboard-";
var INLINE_KEYBOARD_SCREEN_CLASS = "screen-generated-inline-keyboard";
var inline_keyboard_classes = [];

var TEXT_MESSAGE_TYPE = 'text';
var AUDIO_MESSAGE_TYPE = 'audio';
var IMAGE_MESSAGE_TYPE = 'image';
var INLINE_KEYBOARD_MESSAGE_TYPE = 'inlinekeyboard';

var input_id_counter = 1000;

$(document).ready(function () {
    //Fix clone() method to include textarea values
    //$.getScript('jquery.fix.textarea-clone.js', function(){});

    $('#text-msg, #image-msg, #audio-msg, #file-msg, #keyboard-container').draggable({
        cursor: 'move', // sets the cursor apperance
        revert: 'invalid',// makes the item to return if it isn't placed into droppable
        revertDuration: 500,// duration while the item returns to its place
        helper: "clone",
        connectToSortable: "#screen-body ul",
        addClasses: false
    });

    $('#screen-body ul').sortable();
    $('#screen-body ul').disableSelection();

    $('#screen-body').droppable({
        tolerance: "intersect",
        accept: "#text-msg, #image-msg, #audio-msg",
        drop: function(event,ui){
            newElement(ui);
        }
    });

    $('#keyboard-screen-container').droppable({
        tolerance: "intersect",
        accept: "#keyboard-container",
        drop: function(event,ui){
            newKeyboard(ui);
        }
    });

    //////Events
    //Screen frame switcher event
    $('input:radio[name=smartphone-switcher]').change(function(){
        if(this.value == 'android'){
            $('#top-screen-frame').removeClass("top-screen-frame-iphone");
            $('#top-screen-frame').addClass("top-screen-frame-android");

            $('#middle-screen-frame').removeClass("middle-screen-frame-iphone");
            $('#middle-screen-frame').addClass("middle-screen-frame-android");

            $('#bottom-screen-frame').removeClass("bottom-screen-frame-iphone");
            $('#bottom-screen-frame').addClass("bottom-screen-frame-android");

        }else if(this.value == 'iphone'){
            $('#top-screen-frame').removeClass("top-screen-frame-android");
            $('#top-screen-frame').addClass("top-screen-frame-iphone");

            $('#middle-screen-frame').removeClass("middle-screen-frame-android");
            $('#middle-screen-frame').addClass("middle-screen-frame-iphone");

            $('#bottom-screen-frame').removeClass("bottom-screen-frame-android");
            $('#bottom-screen-frame').addClass("bottom-screen-frame-iphone");
        }
    });

    //Inline Keyboard generator events
    $('#inline-keyboard-add-row button').click(function(){
        var rows = parseInt($('.generated-inline-keyboard .keyboard-row').length);
        var next_row = rows++;
        $('.generated-inline-keyboard').append('<div id="row-' + next_row + '" class="keyboard-row"></div>');
        $('#inline-keyboard-buttons-panel').append('<button id="button-' + next_row + '" class="add-button" >+</button>');
    });

    $('#reset-inline-keyboard button').click(function(){
        $('.generated-inline-keyboard').html('');
        $('#inline-keyboard-buttons-panel').html('');
    });

    //click event for all row buttons
    $('#inline-keyboard-generator').on('click','.add-button',function(){
        input_id_counter = input_id_counter + 1;
        var button_number = $(this).attr('id').split('-')[1];
        console.log('Button number: ' + button_number);
        $('.generated-inline-keyboard #row-'+ button_number).append('<div class="inline-keyboard-button" ><input id="input-' + input_id_counter + '" ></div>');
    });

    $('#add-inline-keyboard').click(function(){
        /*var next_class_number = 1;
        var current_class_number = 1;
        var next_class = INLINE_KEYBOARD_BASE_CLASS + next_class_number;
        if(inline_keyboard_classes.length > 0){
            current_class_number = inline_keyboard_classes[inline_keyboard_classes.length-1];
            next_class_number = current_class_number + 1;
            next_class = INLINE_KEYBOARD_BASE_CLASS + next_class_number;
        }
        */
        var keyboard_item = $("<li class='message-inlinekeyboard'></li>").html($('.'+INLINE_KEYBOARD_GENERATOR_CLASS).clone().addClass(INLINE_KEYBOARD_SCREEN_CLASS).removeClass(INLINE_KEYBOARD_GENERATOR_CLASS));
        //inline_keyboard_classes.push(next_class_number);
        keyboard_item.appendTo($('#message-ul'));
        $("."+next_class).find("input").prop('disabled', true);
    });

    $('#keyboard-generator #remove-screen-keyboard').click(function(){
        $('#keyboard-screen-container').html('');
    });

    //Normal Keyboard generator events
    $('#add-row button').click(function(){
        console.log("Row added");
        var rows = parseInt($('.generated-keyboard .keyboard-row').length);
        var next_row = rows++;
        console.log("Row number: " + next_row);
        $('.generated-keyboard').append('<div id="row-' + next_row + '" class="keyboard-row"></div>');
        $('#buttons-panel').append('<button id="button-' + next_row + '" class="add-button" >+</button>');
    });

    $('#reset-keyboard button').click(function(){
        $('.generated-keyboard').html('');
        $('#buttons-panel').html('');
    });

    //click event for all row buttons
    $('#common-keyboard-generator').on('click','.add-button',function(){
        console.log("New button");
        input_id_counter = input_id_counter + 1;
        var row_number = $(this).attr('id').split('-')[1];
        console.log('Button number: ' + input_id_counter);
        $('.generated-keyboard #row-'+ row_number).append('<div class="keyboard-button" ><input id="input-' + input_id_counter + '" ></div>');
    });

    $('#add-keyboard').click(function(){
        $('#keyboard-screen-container').html($('.generated-keyboard').clone().addClass(COMMON_KEYBOARD_SCREEN_CLASS).removeClass(COMMON_KEYBOARD_GENERATOR_CLASS));
        $("#keyboard-screen-container input").prop('disabled', true);
    });

    $('#remove-screen-keyboard').click(function(){
        $('#keyboard-screen-container').html('');
    });

    //Image picker event
    $('#image-picker').change(function(){
        console.log("Image was picked!");
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image-msg img').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    //Screenshot button click event
    $('#screenshot-button').click(function(){
        console.log("screenshot button clicked!");
        html2canvas(document.querySelector("#screen")).then(canvas => {
            console.log("screenshot created!");
            $('#canvas-container').empty();
            $('#canvas-container').append(canvas);
            console.log("URL: "+canvas.toDataURL("image/png"));
            $('#screen-img').val(canvas.toDataURL("image/png"));
            $('#screen-img-form').submit();
        });
    });

    //Save html button click event
    $('#save-html-button').click(function(){
        //Create json structure
        var screen = {
            html: $('#screen').html(),
            input_id_count: input_id_counter,
            inputs: []
        };

        $("#screen :input").each(function(){
            var input_dict = {};
            input_dict['id'] = $(this).attr('id');
            input_dict['value'] = $(this).val();
            screen.inputs.push(input_dict);
        });
        console.log("JSON: ");
        console.log(JSON.stringify(screen));

        $('#screen-json-data').val(JSON.stringify(screen));

        //sleep(50000);

        //Save screen image
        html2canvas(document.querySelector("#screen")).then(canvas => {
            console.log("screenshot created!");
            $('#canvas-container').empty();
            $('#canvas-container').append(canvas);
            $('#screen-img-save').val(canvas.toDataURL("image/png"));
            $('#screen-html-form').submit();
        });

    });

    //Change colors events
    $('#background-color-picker').change(function(){
        console.log("Background color changed");
        console.log("Color: " + $(this).val());
        $('#screen-body').css("background-color", $(this).val());
    });

    $('#menu-color-picker').change(function(){
        $('#screen-header').css("background-color", $(this).val());
    });

    $('#thumbnail-color-picker').change(function(){
        $('#chat-thumbnail').css("background-color", $(this).val());
    });

    $('#screen-preview-button').click(function(){
        $('#preview-screen-modal').html($('#screen').html());
        $('#exampleModal').modal('show');
    });

    //Add screen item toolbar when hover
    $('#message-ul').on("mouseenter", "li", function() {
        var toolbar_html = '<span class="screen-item-toolbar" ><i id="screen-drag" class="small material-icons not-drag">drag_indicator</i><i id="screen-delete" class="material-icons delete" >delete</i></span>';
        $(toolbar_html).appendTo(this);
    });

    $('#message-ul').on("mouseleave", "li",function(){
        $(this).children(".screen-item-toolbar").remove();
    });

    //Remove li elements from screen ul
    $('#message-ul').on("click", ".delete", function(){
        $(this).parent().parent().remove();
    });

    //Event for project and screen remove modal window accept button
    $('#remove-project-accept').click(function(){
        $('#remove-project-modal').modal('hide')
        $('#remove-project-form').submit();
    });

    $('#remove-screen-accept').click(function(){
        $('#remove-screen-modal').modal('hide')
        $('#remove-screen-form').submit();
    });

    //////////End events

    // Option to make auto-grow all textareas
    $('textarea').autogrow();

});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function newElement(ui){
    input_id_counter = input_id_counter + 1;
    console.log("Draggable type: " + ui.helper.text());
    console.log("Top: " + ui.position.top);
    console.log("Left: " + ui.position.left);
    console.log(ui.helper);
    console.log("Input id: " + input_id_counter);
    var element_type = ui.draggable.prop('id').split('-')[0];
    console.log("Element type: " + element_type);
    if(element_type == TEXT_MESSAGE_TYPE){
        if($('input:radio[name=conversation-side-switcher]:checked').val() == 'left'){
            ($('<li class="screen-message message-left" ></li>').html(ui.helper.find("textarea").attr({'id': 'input-' + input_id_counter, 'disabled': true}))).appendTo($('#message-ul'));
        }else{
            ($('<li class="screen-message message-right" ></li>').html(ui.helper.find("textarea").attr({'id': 'input-' + input_id_counter, 'disabled': true}))).appendTo($('#message-ul'));
        }
    }else if(element_type == INLINE_KEYBOARD_MESSAGE_TYPE){
        if($('input:radio[name=conversation-side-switcher]:checked').val() == 'left'){
            ($('<li class="screen-message message-left inline-keyboard-message" ></li>').html(ui.helper.find("textarea").attr({'id': 'input-' + input_id_counter, 'disabled': true}))).appendTo($('#message-ul'));
        }else{
            ($('<li class="screen-message message-right inline-keyboard-message" ></li>').html(ui.helper.find("textarea").attr({'id': 'input-' + input_id_counter, 'disabled': true}))).appendTo($('#message-ul'));
        }
    }else if(element_type == IMAGE_MESSAGE_TYPE){
        if($('input:radio[name=conversation-side-switcher]:checked').val() == 'left'){
            ($('<li class="screen-image-message message-left" ></li>').html(ui.helper.find("img"))).appendTo($('#message-ul'));
        }else{
            ($('<li class="screen-image-message message-right" ></li>').html(ui.helper.find("img"))).appendTo($('#message-ul'));
        }
    }else
    {
        ($('<li class="screen-message message-right" ></li>').html(ui.helper.children())).appendTo($('#message-ul'));
    }
}

function newKeyboard(ui){
    input_id_counter = input_id_counter + 1;
    $('#keyboard-screen-container').html(ui.helper.html().find("input").attr({'id': 'input-' + input_id_counter, 'disabled': true}));
}


//stop: function (event, ui) {
    //if (ui.position.left - extraMargin <= boardLeftMargin + kPixelWidth - kPieceWidth && (ui.position.top + kPieceHeight - extraMargin <= boardTopMargin + kPixelHeight && ui.position.top + extraMargin>= boardTopMargin)) {
        //var x = Math.floor(((parseInt(ui.position.left) - boardLeftMargin) + kPieceWidth / 2) / kPieceWidth) * kPieceWidth;
        //var y = Math.floor(((parseInt(ui.position.top) - boardTopMargin) + kPieceHeight / 2) / kPieceHeight) * kPieceHeight;
        //$('#ui').append("<span>" + this.id + " -> " + x / kPieceWidth + ":" + y / kPieceHeight + "</span><br>");
        //gDrawingContext.drawImage(this, x, y);
//    }
